import axios from "../api/axios";
import { toast } from "react-toastify";

axios.interceptors.response.use(null, error => {
    const errorResponse = error.response;
    const expectedError = errorResponse && errorResponse.status >= 400 && errorResponse.status < 500;
    if (!expectedError) {
        console.log('Logging Error', error)
        toast.error("An unexpected error occured.");
    }
    else if (errorResponse.status === 400) {
        toast.error('Bad Request')
    }
    else if (errorResponse.status === 404) {
        toast.error('Requested Resource Not Found.')
    }

    return Promise.reject(error)
})

const http = {
    get: axios.get,
    post: axios.post,
    put: axios.put,
    delete: axios.delete
}

export default http;