import http from "./http-service";

export function index(){
    return http.get('/vehicles')
}

export function store(data){
    return http.post('/vehicles', data)
}

export function show(id){
    return http.get('/vehicles/'+id)
}

export function update(id, data){
    return http.put('/vehicles/'+id, data)
}

export function destroy(id){
    return http.delete('/vehicles/'+id)
}