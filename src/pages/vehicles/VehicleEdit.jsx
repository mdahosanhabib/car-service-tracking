import { Form, Formik } from "formik";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Input from "../../components/forms/Input";
import UpdateButton from "../../components/forms/UpdateButton";
import Master from "../../components/layouts/Master";
import Card from "../../components/utilities/Card";
import ListLink from "../../components/utilities/ListLink";
import { show, update } from "./../../service/vehicle-service";
import { vehicleSchema } from "../../validations/VehicleValidation";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const VehicleEdit = () => {
  const navigate = useNavigate();
  const [vehicle, setVehicle] = useState({
    name: "",
    model: "",
    registration_no: "",
    color: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const { id } = useParams();

  useEffect(() => {
    getSingleData();
  }, []);

  const getSingleData = async () => {
    const response = await show(id);
    setVehicle(response.data);
    setIsLoading(false);
  };

  const handleSubmit = async (values) => {
    await update(id, values);
    toast.success('Successfully Updated');
    navigate("/vehicles");
  };

  if (isLoading) {
    return (
      <section>
        <p>Loading</p>
      </section>
    );
  }

  return (
    <Master>
      <Card header="Edit Vehicle">
        <ListLink url="/vehicles" />
        <br />
        <br />
        <Formik
          enableReinitialize={true}
          initialValues={{
            name: vehicle.name,
            model: vehicle.model,
            registration_no: vehicle.registration_no,
            color: vehicle.color,
          }}
          validationSchema={vehicleSchema}
          onSubmit={(values) => {
            handleSubmit(values);
          }}
        >
          {(formik) => (
            <Form>
              <Input label="Name" name="name" type="text" />
              <Input label="Model" name="model" type="text" />
              <Input
                label="Registration No"
                name="registration_no"
                type="text"
              />
              <Input label="Color" name="color" type="text" />
              <div className="text-center lg:text-left">
                <UpdateButton type="submit" />
              </div>
            </Form>
          )}
        </Formik>
      </Card>
      <ToastContainer />
    </Master>
  );
};

export default VehicleEdit;
