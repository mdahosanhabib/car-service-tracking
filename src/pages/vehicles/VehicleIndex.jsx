import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import DeleteButton from "../../components/forms/DeleteButton";
import Master from "../../components/layouts/Master";
import Card from "../../components/utilities/Card";
import CreateLink from "../../components/utilities/CreateLink";
import EditLink from "../../components/utilities/EditLink";
import ShowLink from "../../components/utilities/ShowLink";
import { index, destroy } from "./../../service/vehicle-service";

function VehicleIndex() {
  const [isLoading, setIsLoading] = useState(true);
  const [vehicles, setVehicles] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const { data } = await index();
    setVehicles(data);
    setIsLoading(false);
  };

  const onDelete = async (id) => {
    if (window.confirm("Are You Sure Want to Delete")) {
      setIsLoading(true);
      await destroy(id);
      toast.success("Successfully Deleted");
      getData();
    }
  };

  if (isLoading) {
    return (
      <section>
        <p>Loading</p>
      </section>
    );
  }

  return (
    <Master>
      <Card header="Vehicles">
        <CreateLink url="/vehicles/create" />
        <br />
        <br />
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" className="px-6 py-3">
                  Name
                </th>
                <th scope="col" className="px-6 py-3">
                  Model
                </th>
                <th scope="col" className="px-6 py-3">
                  Registration No
                </th>
                <th scope="col" className="px-6 py-3">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {console.log("===========")}
              {console.log(vehicles)}
              {console.log("------------")}
              {vehicles.map((vehicle) => {
                return (
                  <tr
                    key={vehicle._id}
                    className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
                  >
                    <td className="px-6 py-4">{vehicle.name}</td>
                    <td className="px-6 py-4">{vehicle.model}</td>
                    <td className="px-6 py-4">{vehicle.registration_no}</td>
                    <td className="px-6 py-4">
                      <ShowLink url={"/vehicles/" + vehicle._id} />
                      <EditLink url={"/vehicles/" + vehicle._id + "/edit"} />
                      <DeleteButton handleClick={() => onDelete(vehicle._id)} />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </Card>
      <ToastContainer />
    </Master>
  );
}

export default VehicleIndex;
