import { useNavigate } from "react-router-dom";
import CreateButton from "../../components/forms/CreateButton";
import Input from "../../components/forms/Input";
import Master from "../../components/layouts/Master";
import Card from "../../components/utilities/Card";
import ListLink from "../../components/utilities/ListLink";
import { vehicleSchema } from "../../validations/VehicleValidation";
import { Formik, Form } from "formik";
import { store } from "../../service/vehicle-service";
// import {
//   faCheck,
//   faTimes,
//   faInfoCircle,
// } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const VehicleCreate = () => {
  const navigate = useNavigate();

  const handleSubmit = async(values) => {
    await store(values);
    navigate('/vehicles')
  }

  return (
    <Master>
      <Card header="Create Vehicle">
        <ListLink url="/vehicles" />
        <br /><br />
        <Formik
          initialValues={{
            name: "",
            model: "",
            registration_no: "",
            color: "",
          }}
          validationSchema={vehicleSchema}
          onSubmit={(values) => {
            handleSubmit(values);
          }}
        >
          {(formik) => (
            <Form>
              <Input label="Name" name="name" type="text" />
              <Input label="Model" name="model" type="text" />
              <Input
                label="Registration No"
                name="registration_no"
                type="text"
              />
              <Input label="Color" name="color" type="text" />
              <div className="text-center lg:text-left">
                <CreateButton type="submit" />
              </div>
            </Form>
          )}
        </Formik>
      </Card>
    </Master>
  );
};

export default VehicleCreate;
