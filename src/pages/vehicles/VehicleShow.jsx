import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "../../api/axios";
import Master from "../../components/layouts/Master";
import Card from "../../components/utilities/Card";
import ListLink from "../../components/utilities/ListLink";

const VEHICLE_LIST_URL = "vehicles";

function VehicleShow() {
  const [vehicle, setVehicle] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const { id } = useParams();

  useEffect(() => {
    getSingleData();
  }, []);

  const getSingleData = async () => {
    const response = await axios.get(`${VEHICLE_LIST_URL}/${id}`);
    if (response.status === 200) {
      setVehicle(response.data);
      setIsLoading(false);
    }
  };

  if (isLoading) {
    return (
      <section>
        <p>Loading</p>
      </section>
    );
  }

  return (
    <Master>
      <Card header="Show Vehicle">
        <ListLink url="/vehicles" />
        <br /><br />
        <h3>Name: {vehicle?.name}</h3>
        <p>Model: {vehicle?.model}</p>
        <p>Registration No: {vehicle?.registration_no}</p>
        <p>Color: {vehicle?.color}</p>
      </Card>
    </Master>
  );
}

export default VehicleShow;
