import { useNavigate } from "react-router-dom";
import { Form, Formik } from "formik";
import { useContext } from "react";
import { login } from "../service/auth-service";
import CreateButton from "../components/forms/CreateButton";
import Input from "../components/forms/Input";
import { authSchema } from "../validations/authValidation";
const { AuthContext } = require("../context/AuthContext");

function Login() {
  const navigate = useNavigate();
  const { user, loading, error, loginStart, loginSuccess, loginFailure } =
    useContext(AuthContext);

  if (user) {
    window.location.href = "/home";
  }

  const handleSubmit = async (values) => {
    loginStart();
    try {
      const { data } = await login(values);
      console.log("data");
      loginSuccess(data);
      navigate("/home");
    } catch (err) {
      console.log(err);
      loginFailure(err);
    }
  };

  return (
    <section className="h-screen">
      <div className="px-6 h-full text-gray-800">
        <div className="flex xl:justify-center lg:justify-between justify-center items-center flex-wrap h-full g-6">
          <div className="grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 mb-12 md:mb-0">
            <img
              src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
              className="w-full"
              alt="Sample image"
            />
          </div>
          <div className="xl:ml-20 xl:w-5/12 lg:w-5/12 md:w-8/12 mb-12 md:mb-0">
            <Formik
              initialValues={{
                username: "",
                password: "",
              }}
              validationSchema={authSchema}
              onSubmit={(values) => {
                handleSubmit(values);
              }}
            >
              {(formik) => (
                <Form>
                  <Input label="Username" name="username" type="text" />
                  <Input label="Password" name="password" type="password" />
                  <div className="text-center lg:text-left">
                    <CreateButton disabled={loading} type="submit" />
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Login;
