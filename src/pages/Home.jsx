import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";

import events from "./../events";
import CarServiceStatus from "../components/CarServiceStatus";
import Master from "../components/layouts/Master";

function Home() {
  const [isLoading, setIsLoading] = useState(true);
  const [vehicles, setVehicles] = useState([]);
  const [vehicle, setVehicle] = useState(false);

  useEffect(() => {
    getVehicles();
  }, []);

  const getVehicle = async (id) => {
    try {
      const response = await fetch("http://localhost:8081/vehicles/" + id);
      const data = await response.json();
      console.log(data);
      setVehicle(data);
    } catch (error) {
      console.log("error", error);
    }
  };

  const getVehicles = async () => {
    try {
      const response = await fetch("http://localhost:8081/vehicles");
      const data = await response.json();
      setVehicles(data);
      setIsLoading(false);
    } catch (error) {
      console.log("error", error);
    }
  };

  if (isLoading) {
    return (
      <section>
        <p>Loading</p>
      </section>
    );
  }

  return (
    <Master>
      <div className="flex flex-wrap">
        <div className="w-full md:w-2/5 md:pl-0  p-2">
          {/*Metric Card*/}
          <div className="bg-white border rounded shadow p-2">
            <div className="flex flex-row items-center">
              <div className="flex-1">
                <h5 className="font-bold uppercase text-gray-500 mb-2">
                  CAR SERVICE TRACKING AND WORKFLOW MANAGEMENT
                </h5>
                <div className="flex flex-row">
                  <div className="w-full md:w-1/2 xl:w-1/2 p-3">
                    <div className="flex flex-row">
                      <p className="w-10/12 font-semibold">Ongoing Servicing</p>
                      <span>:</span>
                      <span className="text-xs inline-block py-1 px-2.5 leading-none text-center whitespace-nowrap align-middle font-bold bg-blue-600 text-white rounded">
                        2
                      </span>
                    </div>

                    <div className="flex flex-row ">
                      <p className="w-10/12 font-semibold">
                        Waiting for Service
                      </p>
                      <span>:</span>
                      <span className="text-xs inline-block py-1 px-2.5 leading-none text-center whitespace-nowrap align-baseline font-bold bg-blue-600 text-white rounded">
                        2
                      </span>
                    </div>

                    <div className="flex flex-row">
                      <p className="w-10/12 font-semibold">
                        Servicing Completed
                      </p>
                      <span>:</span>
                      <span className="text-xs inline-block py-1 px-2.5 leading-none text-center whitespace-nowrap align-baseline font-bold bg-blue-600 text-white rounded">
                        2
                      </span>
                    </div>
                  </div>

                  <div className="w-full md:w-1/2 xl:w-1/2 p-3">
                    <div className="flex flex-row">
                      <p className="w-10/12 font-semibold">Car Checking</p>
                      <span>:</span>
                      <span className="text-xs inline-block py-1 px-2.5 leading-none text-center whitespace-nowrap align-baseline font-bold bg-blue-600 text-white rounded">
                        2
                      </span>
                    </div>

                    <div className="flex flex-row ">
                      <p className="w-10/12 font-semibold">
                        Waiting for Checking
                      </p>
                      <span>:</span>
                      <span className="text-xs inline-block py-1 px-2.5 leading-none text-center whitespace-nowrap align-baseline font-bold bg-blue-600 text-white rounded">
                        2
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/*/Metric Card*/}
        </div>
        <div className="w-full md:w-3/5  md:pr-0 p-2">
          <div className="bg-white border rounded shadow p-2">
            <div className="flex flex-row items-center">
              <div className="flex-1">
                <h5 className="font-bold uppercase text-gray-500">
                  Ongoing Processes
                </h5>

                <div className="w-full p-3">
                  <div className="flex align-baseline space-x-1">
                    <p className="w-1/4 font-semibold">Car #1 (121323)</p>
                    <span>:</span>
                    <div className="w-2/4 mt-2 ml-2 bg-gray-200 rounded-full h-2.5">
                      <div
                        className="bg-green-600 h-2.5 rounded-full"
                        style={{ width: "45%" }}
                      ></div>
                    </div>

                    <div className="w-1/4 text-center text-blue-500">
                      <p>Details</p>
                    </div>

                    <div className="w-1/4 bg-black-50">
                      <span>Assigned at 8PM</span>
                    </div>
                  </div>
                  <div className="flex align-baseline space-x-1">
                    <p className="w-1/4 font-semibold">Car #1 (121323)</p>
                    <span>:</span>
                    <div className="w-2/4 mt-2 ml-2 bg-gray-200 rounded-full h-2.5">
                      <div
                        className="bg-green-600 h-2.5 rounded-full"
                        style={{ width: "45%" }}
                      ></div>
                    </div>

                    <div className="w-1/4 text-center text-blue-500">
                      <p>Details</p>
                    </div>

                    <div className="w-1/4 bg-black-50">
                      <span>Assigned at 8PM</span>
                    </div>
                  </div>
                  <div className="flex align-baseline space-x-1">
                    <p className="w-1/4 font-semibold">Car #1 (121323)</p>
                    <span>:</span>
                    <div className="w-2/4 mt-2 ml-2 bg-gray-200 rounded-full h-2.5">
                      <div
                        className="bg-green-600 h-2.5 rounded-full"
                        style={{ width: "45%" }}
                      ></div>
                    </div>

                    <div className="w-1/4 text-center text-blue-500">
                      <p>Details</p>
                    </div>

                    <div className="w-1/4 bg-black-50">
                      <span>Assigned at 8PM</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr className="border-b-2 border-gray-400" />
      <div className="flex flex-row flex-wrap flex-grow mt-2 bg-white border shadow">
        <div className="w-full md:w-1/4">
          <div className="border">
            <CarServiceStatus vehicles={vehicles} getVehicle={getVehicle} />
          </div>
        </div>
        <div className="w-full md:w-1/2">
          <div className="bg-white border shadow">
            <div className="p-5">
              <FullCalendar
                initialView="dayGridMonth"
                headerToolbar={{
                  left: "prev,next",
                  center: "title",
                  right: "dayGridMonth,timeGridWeek,timeGridDay",
                }}
                plugins={[dayGridPlugin, timeGridPlugin]}
                events={events}
                editable={true}
              />
            </div>
          </div>
          {/*/Graph Card*/}
        </div>
        <div className="w-full md:w-1/4">
          <div className="bg-white border shadow">
            <div className="p-3">
              {vehicle && (
                <table className="table-auto p-3">
                  <tbody>
                    <tr>
                      <td>Car Identification Number</td>
                      <td>{vehicle._id}</td>
                    </tr>
                    <tr>
                      <td>Car Registration Number</td>
                      <td>{vehicle.registration_no}</td>
                    </tr>
                    <tr>
                      <td>Total Problems</td>
                      <td>25</td>
                    </tr>
                    <tr>
                      <td>Service Advisor</td>
                      <td>Mr X</td>
                    </tr>
                    <tr>
                      <td>Total Technicians</td>
                      <td>4</td>
                    </tr>
                    <tr>
                      <td>Deadline</td>
                      <td>4 Dec</td>
                    </tr>
                  </tbody>
                </table>
              )}
            </div>

            <div className="border-b p-3">
              <h5 className="font-bold uppercase text-gray-600">
                Car Services
              </h5>
            </div>
            <ul className="text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white">
              <li className="w-full px-4 py-2 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                <h5>Car #11</h5>
                <div className="flex flex-row flex-wrap">
                  <p className="text-xs">Problems: 20</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Solved: 15</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Ongoing: 5</p>
                </div>
              </li>
              <li className="w-full px-4 py-2 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                <h5>Car #11</h5>
                <div className="flex flex-row flex-wrap">
                  <p className="text-xs">Problems: 20</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Solved: 15</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Ongoing: 5</p>
                </div>
              </li>
              <li className="w-full px-4 py-2 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                <h5>Car #11</h5>
                <div className="flex flex-row flex-wrap">
                  <p className="text-xs">Problems: 20</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Solved: 15</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Ongoing: 5</p>
                </div>
              </li>
              <li className="w-full px-4 py-2 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                <h5>Car #11</h5>
                <div className="flex flex-row flex-wrap">
                  <p className="text-xs">Problems: 20</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Solved: 15</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Ongoing: 5</p>
                </div>
              </li>
              <li className="w-full px-4 py-2 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                <h5>Car #11</h5>
                <div className="flex flex-row flex-wrap">
                  <p className="text-xs">Problems: 20</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Solved: 15</p>
                  <span className="text-xs mx-1">|</span>
                  <p className="text-xs">Ongoing: 5</p>
                </div>
              </li>
            </ul>
          </div>
          {/*/Graph Card*/}
        </div>
      </div>
    </Master>
  );
}

export default Home;
