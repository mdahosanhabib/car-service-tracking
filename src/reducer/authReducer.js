const LOGIN_START = "LOGIN_START";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const LOGOUT = "LOGOUT";

export const createActions = dispatch => ({
    loginStart: value => dispatch({ type: LOGIN_START, payload: value }),
    loginSuccess: value => dispatch({ type: LOGIN_SUCCESS, payload: value }),
    loginFailure: value => dispatch({ type: LOGIN_FAILURE, payload: value }),
    logout: value => dispatch({ type: LOGOUT, payload: value })
})

const AuthReducer = (state, action) => {
    switch (action.type) {
        case LOGIN_START:
            return {
                user: null,
                loading: true,
                error: null,
            };
        case LOGIN_SUCCESS:
            return {
                user: action.payload,
                loading: false,
                error: null,
            };
        case LOGIN_FAILURE:
            return {
                user: null,
                loading: false,
                error: action.payload,
            };
        case LOGOUT:
            return {
                user: null,
                loading: false,
                error: null,
            };
        default:
            return state;
    }
};

export default AuthReducer;