import { useContext } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
// import Footer from './components/Footer';
import Navbar from './components/layouts/partials/Navbar';
import { AuthContext } from "./context/AuthContext";
import Home from './pages/Home';
import Login from './pages/Login';
import VehicleCreate from "./pages/vehicles/VehicleCreate";
import VehicleEdit from "./pages/vehicles/VehicleEdit";
import VehicleIndex from "./pages/vehicles/VehicleIndex";
import VehicleShow from "./pages/vehicles/VehicleShow";

function App() {
  const { user } = useContext(AuthContext);

  const ProtectedRoute = ({ children }) => {
    if (!user) {
      return <Navigate to="/" />;
    }
    return children;
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home" element={
          <ProtectedRoute><Home /></ProtectedRoute>} />
        <Route path="/vehicles" element={
          <ProtectedRoute><VehicleIndex /></ProtectedRoute>} />
        <Route path="/vehicles/create" element={
          <ProtectedRoute><VehicleCreate /></ProtectedRoute>} />
        <Route path="/vehicles/:id" element={
          <ProtectedRoute><VehicleShow /></ProtectedRoute>} />
        <Route path="/vehicles/:id/edit" element={
          <ProtectedRoute><VehicleEdit /></ProtectedRoute>} />
        <Route path="/problems" element={
          <ProtectedRoute><VehicleIndex /></ProtectedRoute>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
