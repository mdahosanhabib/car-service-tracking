import React from "react";
import { useEffect } from "react";
import { useState } from "react";

function SingleCarStatus({ sl, id, getVehicle }) {
  return (
    <li
      className="w-full px-4 py-2 border-b border-gray-200 rounded-t-lg dark:border-gray-600"
      onClick={(e) => getVehicle(id)}
    >
      <h5>Car #{sl}</h5>
      <div className="flex flex-row flex-wrap">
        <p className="text-xs">Problems: 20</p>
        <span className="text-xs mx-1">|</span>
        <p className="text-xs">Solved: 15</p>
        <span className="text-xs mx-1">|</span>
        <p className="text-xs">Ongoing: 5</p>
      </div>
    </li>
  );
}

export default SingleCarStatus;
