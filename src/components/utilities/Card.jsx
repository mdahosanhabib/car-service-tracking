import React from "react";

function Card({ header, footer, children }) {
  return (
    <div className="block rounded-lg shadow-lg bg-white w-full">
      {header && (
        <div className="py-3 px-6 border-b border-gray-300">{header}</div>
      )}
      <div className="p-6">{children}</div>
      {footer && (
        <div className="py-3 px-6 border-t border-gray-300 text-gray-600">
          {footer}
        </div>
      )}
    </div>
  );
}

export default Card;
