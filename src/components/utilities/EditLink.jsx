import React from "react";
import { Link } from "react-router-dom";

function EditLink({url}) {
  return (
    <Link
      to={url}
      className="inline-block px-6 py-2.5 bg-yellow-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-yellow-700 hover:shadow-lg focus:bg-yellow-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-yellow-800 active:shadow-lg transition duration-150 ease-in-out"
    >
      Edit
    </Link>
  );
}

export default EditLink;
