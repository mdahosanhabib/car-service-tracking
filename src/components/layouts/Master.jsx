import React from "react";
import Footer from "./partials/Footer";
import Navbar from "./partials/Navbar";

function Master({ children }) {
  return (
    <>
      <Navbar />
      <div className="container w-full mx-auto pt-20">
        <div className="w-full px-4 md:px-0 md:mt-8 mb-16 text-gray-800 leading-normal">
          {children}
        </div>
      </div>
      <Footer />
    </>
  );
}

export default Master;
