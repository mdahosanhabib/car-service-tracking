import React from "react";
import SingleCarStatus from "./SingleCarStatus";

function CarServiceStatus(props) {
  return (
    <ul className="text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white">
      {props.vehicles.map((vehicle, index) => (
        <SingleCarStatus key={vehicle._id} id={vehicle._id} sl={index + 1} getVehicle={props.getVehicle} />
      ))}
    </ul>
  );
}

export default CarServiceStatus;
