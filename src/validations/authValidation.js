import * as yup from "yup"

export const authSchema = yup.object().shape({
    username: yup.string().max(100).required(),
    password: yup.string().required(),
})
