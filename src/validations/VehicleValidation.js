import * as yup from "yup"

export const vehicleSchema = yup.object().shape({
    name: yup.string().min(4).max(100).required(),
    model: yup.string().required(),
    registration_no: yup.string().required() 
})
